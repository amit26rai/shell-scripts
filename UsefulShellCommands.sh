# list out the size of each file and directory (recursively) and sort by size decendingly
#https://stackoverflow.com/questions/7463554/how-can-i-list-out-the-size-of-each-file-and-directory-recursively-and-sort-by
du -a -h --max-depth=1 | sort -hr